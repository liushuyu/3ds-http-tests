#include "common.h"

#include "cJSON.h"
#include <stdlib.h>

#define SPEED_TEST_API_URL                                                     \
  "https://api.fast.com/netflix/speedtest/"                                    \
  "v2?https=true&token=YXNkZmFzZGxmbnNkYWZoYXNkZmhrYWxm&urlCount=3"
// Utility functions
Result find_speed_test_url(const u8 *json_str, size_t json_len, char **url) {
  cJSON *targets, *test_file, *test_url = NULL;
  Result ret = 0;
  cJSON *json = cJSON_ParseWithLength((const char *)json_str, json_len);
  if (!json) {
    ret = -1;
    goto end;
  }
  targets = cJSON_GetObjectItemCaseSensitive(json, "targets");
  if (!cJSON_IsArray(targets) || (cJSON_GetArraySize(targets) < 1)) {
    ret = -2;
    goto end;
  }
  test_file = cJSON_GetArrayItem(targets, 0);
  if (!test_file) {
    ret = -3;
    goto end;
  }
  test_url = cJSON_GetObjectItemCaseSensitive(test_file, "url");
  if (!url)
    ret = -4;
  *url = strdup(cJSON_GetStringValue(test_url));
end:
  cJSON_Delete(json);
  return ret;
}

// http_fetch minus all the checks and output
Result speed_test_http_fetch(const char *url, double *bps) {
  Result ret = 0;
  TickCounter counter;
  httpcContext context;
  u32 statuscode = 0;
  u32 readsize = 0, size = 0;
  u8 *buf;
  buf = malloc(0x100000); // allocate a larger amount of buffer (10 MB)

  if (!buf) {
    printf("Failed to allocate buffer\n");
    return -1;
  }
  ret = httpcOpenContext(&context, HTTPC_METHOD_GET, url, 1);
  ret |= httpcSetSSLOpt(&context, SSLCOPT_DisableVerify);
  ret |= httpcSetKeepAlive(&context, HTTPC_KEEPALIVE_ENABLED);
  ret |= httpcAddRequestHeaderField(&context, "User-Agent", USER_AGENT);
  ret |= httpcAddRequestHeaderField(&context, "Connection", "Keep-Alive");
  ret |= httpcBeginRequest(&context);
  if (ret != 0) {
    goto cleanup;
  }
  ret = httpcGetResponseStatusCode(&context, &statuscode);
  if (ret != 0) {
    goto cleanup;
  }
  if (statuscode < 200 || statuscode > 299) {
    ret = -2;
    goto cleanup;
  }
  osTickCounterStart(&counter);
  do {
    ret = httpcDownloadData(&context, buf, 0x100000, &readsize);
    size += readsize;
  } while (ret == (s32)HTTPC_RESULTCODE_DOWNLOADPENDING);
  osTickCounterUpdate(&counter);
  *bps = size / osTickCounterRead(&counter);

cleanup:
  if (buf != NULL)
    free(buf);
  httpcCloseContext(&context);
  return ret;
}

static Result http_post_data(const char *url, bool binary) {
  Result ret = 0;
  httpcContext context;
  u32 statuscode = 0;
  u32 readsize = 0;
  u8 *buf;
  buf = malloc(0x10000); // allocate a larger amount of buffer (1 MB)
  if (!buf) {
    printf("Failed to allocate buffer\n");
    return -1;
  }
  ret = httpcOpenContext(&context, HTTPC_METHOD_GET, url, 1);
  ret |= httpcSetSSLOpt(&context, SSLCOPT_DisableVerify);
  ret |= httpcSetKeepAlive(&context, HTTPC_KEEPALIVE_ENABLED);
  ret |= httpcAddRequestHeaderField(&context, "User-Agent", USER_AGENT);
  ret |= httpcAddRequestHeaderField(&context, "Connection", "Keep-Alive");
  if (!binary) {
    ret |= httpcAddPostDataAscii(&context, "key1", "value1");
    ret |= httpcAddPostDataAscii(&context, "key2", "value2");
  } else {
    const u8 data[8] = {0, 1, 2, 3, 4, 5, 6, 7};
    ret |= httpcAddPostDataBinary(&context, "bin1", data, 8);
  }
  ret |= httpcBeginRequest(&context);

  if (ret != 0) {
    printf("Error posting data: %" PRId32 "\n", ret);
    goto cleanup;
  }
  ret = httpcGetResponseStatusCode(&context, &statuscode);
  if (ret != 0) {
    goto cleanup;
  }
  if (statuscode < 200 || statuscode > 299) {
    ret = -2;
    goto cleanup;
  }
  do {
    ret = httpcDownloadData(&context, buf, 0x10000, &readsize);
    printf("%s", buf);
  } while (ret == (s32)HTTPC_RESULTCODE_DOWNLOADPENDING);

cleanup:
  if (buf != NULL)
    free(buf);
  httpcCloseContext(&context);
  return ret;
}

char *human_size(const double size) {
  char buf[32];
  if (size > 1024 * 1024) {
    snprintf(buf, sizeof(buf), "%.2f MB", size / 1024.f / 1024.f);
  } else if (size > 1024) {
    snprintf(buf, sizeof(buf), "%.2f KB", size / 1024.f);
  } else {
    snprintf(buf, sizeof(buf), "%.0f B", size);
  }
  return strdup(buf);
}
// ====
void test_simple_http_lan() {
  REQUIRE_TEST_SERVER_ADDR()
  const char *endpoint = "/gen_204";
  struct HTTPResult result;
  size_t buf_size = strlen(endpoint) + strlen(test_server_addr) + 1;
  char *buf = calloc(buf_size, sizeof(char));
  if (buf == NULL) {
    show_message("Error: unable to allocate memory. Press B to abort.", true);
    return;
  }
  snprintf(buf, buf_size, "%s%s", test_server_addr, endpoint);
  printf("Sending request to %s\n", buf);
  if (http_fetch(buf, &result) != 0) {
    printf("Error: HTTP fetch failed: HTTP status %" PRId32 "\n",
           result.status);
  } else if (result.status != 204) {
    printf("Unexpected status code %" PRId32 "\n", result.status);
  } else {
    printf("OK\n");
  }
  free(buf);
  show_message("Press B to return.", false);
}

void test_simple_http() {
  http_download("http://httpbin.org/get", true);
  show_message("Press B to return.", false);
}

void test_simple_https() {
  http_download("https://httpbin.org/get", true);
  show_message("Press B to return.", false);
}

void test_post_http() {
  http_post_data("https://httpbin.org/post", false);
  show_message("Press B to return.", false);
}

void test_post_http_binary() {
  http_post_data("https://httpbin.org/post", true);
  show_message("Press B to return.", false);
}

void test_speed_test_fastcom() {
  struct HTTPResult result;
  char *url;
  Result parse_result;
  double bps = 0;

  printf("Querying FAST.com ...\n");
  if (http_fetch(SPEED_TEST_API_URL, &result) != 0) {
    printf("... fetch failed\n");
    show_message("Press B to return.", false);
    return;
  }
  if ((parse_result = find_speed_test_url(result.body, result.size, &url)) <
      0) {
    printf("... parse failed: %" PRId32 "\n", parse_result);
    show_message("Press B to return.", false);
    return;
  }
  printf("... found test URL: %s\n", url);
  consoleSelect(&top);
  printf("ALERT!\n3DS is concentrating!\nInputs are disabled\n");
  GFX_REFRESH()
  consoleSelect(&bottom);
  printf("Speed test in progress...\n");
  GFX_REFRESH()
  if (speed_test_http_fetch(url, &bps) != 0) {
    printf("... speed test failed\n");
  } else {
    char *human_bps = human_size(bps);
    printf("... speed test completed\nDownload speed: %s/s\n", human_bps);
    free(human_bps);
  }
  consoleSelect(&top);
  consoleClear();
  GFX_REFRESH()
  consoleSelect(&bottom);
  GFX_REFRESH()
  if (url)
    free(url);
  show_message("Press B to return.", false);
  return;
}
