#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"
#include <3ds.h>

PrintConsole top;
PrintConsole bottom;
char *test_server_addr = NULL;

const struct select_menu main_menu[] = {
    {"Simple HTTP", test_simple_http_lan},
    {"Simple HTTP (External)", test_simple_http},
    {"Simple HTTPS (External)", test_simple_https},
    {"POST HTTP (External)", test_post_http},
    {"POST HTTPS Binary (External)", test_post_http_binary},
    {"HTTP Speed Test [Downlink] (FAST.com)", test_speed_test_fastcom},
    {"Set test server address", set_test_server_addr},
    {NULL, NULL}};

int main() {
  gfxInitDefault();
  httpcInit(0); // Buffer size when POST/PUT.

  consoleInit(GFX_TOP, &top);
  consoleInit(GFX_BOTTOM, &bottom);

  // Main loop
  draw_menu(main_menu);

  // Exit services
  httpcExit();
  gfxExit();
  // Clean up
  if (test_server_addr) free(test_server_addr);
  return 0;
}
