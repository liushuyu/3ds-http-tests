#include <3ds.h>

#include "common.h"

Result http_fetch(const char *url, struct HTTPResult *result) {
  Result ret = 0;
  httpcContext context;
  char *newurl = NULL;
  u32 statuscode = 0;
  u32 contentsize = 0, readsize = 0, size = 0;
  u8 *buf, *lastbuf;

  printf("Downloading %s...\n", url);

  do {
    ret = httpcOpenContext(&context, HTTPC_METHOD_GET, url, 1);
    printf("> httpcOpenContext: %" PRId32 "\n", ret);

    // This disables SSL cert verification, so https:// will be usable
    ret = httpcSetSSLOpt(&context, SSLCOPT_DisableVerify);
    printf("> httpcSetSSLOpt: %" PRId32 "\n", ret);

    // Enable Keep-Alive connections
    ret = httpcSetKeepAlive(&context, HTTPC_KEEPALIVE_ENABLED);
    printf("> httpcSetKeepAlive: %" PRId32 "\n", ret);

    // Set a User-Agent header so websites can identify your application
    ret = httpcAddRequestHeaderField(&context, "User-Agent",
                                     USER_AGENT);
    printf("> httpcAddRequestHeaderField: %" PRId32 "\n", ret);

    // Tell the server we can support Keep-Alive connections.
    // This will delay connection teardown momentarily (typically 5s)
    // in case there is another request made to the same server.
    ret = httpcAddRequestHeaderField(&context, "Connection", "Keep-Alive");
    printf("> httpcAddRequestHeaderField: %" PRId32 "\n", ret);

    ret = httpcBeginRequest(&context);
    if (ret != 0) {
      httpcCloseContext(&context);
      if (newurl != NULL)
        free(newurl);
      return ret;
    }

    ret = httpcGetResponseStatusCode(&context, &statuscode);
    if (ret != 0) {
      httpcCloseContext(&context);
      if (newurl != NULL)
        free(newurl);
      return ret;
    }

    if ((statuscode >= 301 && statuscode <= 303) ||
        (statuscode >= 307 && statuscode <= 308)) {
      if (newurl == NULL)
        newurl = (char *)malloc(0x1000); // One 4K page for new URL
      if (newurl == NULL) {
        httpcCloseContext(&context);
        return -1;
      }
      ret = httpcGetResponseHeader(&context, "Location", newurl, 0x1000);
      url = newurl; // Change pointer to the url that we just learned
      printf("redirecting to url: %s\n", url);
      httpcCloseContext(&context); // Close this context before we try the next
    }
  } while ((statuscode >= 301 && statuscode <= 303) ||
           (statuscode >= 307 && statuscode <= 308));

  if (statuscode < 200 || statuscode > 299) {
    printf("URL returned status: %" PRId32 "\n", statuscode);
    httpcCloseContext(&context);
    if (newurl != NULL)
      free(newurl);
    return -2;
  }

  // This relies on an optional Content-Length header and may be 0
  ret = httpcGetDownloadSizeState(&context, NULL, &contentsize);
  if (ret != 0) {
    httpcCloseContext(&context);
    if (newurl != NULL)
      free(newurl);
    return ret;
  }

  printf("reported size: %" PRId32 "\n", contentsize);

  // Start with a single page buffer
  buf = (u8 *)malloc(0x1000);
  if (buf == NULL) {
    httpcCloseContext(&context);
    if (newurl != NULL)
      free(newurl);
    return -1;
  }

  do {
    // This download loop resizes the buffer as data is read.
    ret = httpcDownloadData(&context, buf + size, 0x1000, &readsize);
    size += readsize;
    if (ret == (s32)HTTPC_RESULTCODE_DOWNLOADPENDING) {
      lastbuf = buf; // Save the old pointer, in case realloc() fails.
      buf = (u8 *)realloc(buf, size + 0x1000);
      if (buf == NULL) {
        httpcCloseContext(&context);
        free(lastbuf);
        if (newurl != NULL)
          free(newurl);
        return -1;
      }
    }
  } while (ret == (s32)HTTPC_RESULTCODE_DOWNLOADPENDING);

  if (ret != 0) {
    httpcCloseContext(&context);
    if (newurl != NULL)
      free(newurl);
    free(buf);
    return -1;
  }

  // Resize the buffer back down to our actual final size
  lastbuf = buf;
  buf = (u8 *)realloc(buf, size);
  if (buf == NULL) { // realloc() failed.
    httpcCloseContext(&context);
    free(lastbuf);
    if (newurl != NULL)
      free(newurl);
    return -1;
  }

  printf("downloaded size: %" PRId32 "\n", size);
  if (newurl != NULL)
    free(newurl);
  httpcCloseContext(&context);
  result->body = buf;
  result->size = size;
  result->status = statuscode;

  return ret;
}

Result http_download(const char *url, bool text) {
  u8 *framebuf_top;
  struct HTTPResult result;
  u32 size = 0;

  http_fetch(url, &result);
  size = result.size;

  if (size > (240 * 400 * 3 * 2))
    size = 240 * 400 * 3 * 2;

  GFX_REFRESH()

  if (text) {
    consoleSelect(&top);
    printf("%s\n", result.body);
    GFX_REFRESH()
    consoleSelect(&bottom);
  } else {
    framebuf_top = gfxGetFramebuffer(GFX_TOP, GFX_LEFT, NULL, NULL);
    memcpy(framebuf_top, result.body, size);
  }

  GFX_REFRESH()
  gspWaitForVBlank();

  free(result.body);
  return 0;
}

size_t draw_menu_inner(const struct select_menu *menu, size_t selected) {
  size_t index = 0;
  struct select_menu item;
  while ((item = menu[index]).name != NULL) {
    printf("%s %s\e[0m\n", index == selected ? "\e[36m " : " ", item.name);
    index++;
  }
  return index;
}

void draw_menu(const struct select_menu *menu) {
  size_t selected = 0;
  bool updated = true;
  size_t size = 0;
  while (aptMainLoop()) {
    gspWaitForVBlank();
    hidScanInput();

    if (updated) {
      consoleClear();
      size = draw_menu_inner(menu, selected);
      updated = false;
    }

    u32 kDown = hidKeysDown();
    if (kDown & KEY_START)
      break; // break in order to return to hbmenu
    if (kDown & KEY_DOWN) {
      selected = (selected + 1) % size;
      updated = true;
    }
    if (kDown & KEY_UP) {
      selected = selected < 1 ? size - 1 : selected - 1;
      updated = true;
    }
    if (kDown & KEY_A) {
      void (*func)();
      if ((func = menu[selected].func)) {
        consoleClear();
        func();
        gfxFlushBuffers();
        updated = true;
      } else {
        return;
      }
    }
    if (kDown & KEY_B)
      return;

    if (updated)
      gfxSwapBuffers();
  }
  return;
}

void show_message(const char *message, bool clear) {
  if (clear)
    consoleClear();
  printf("%s\n", message);
  gfxSwapBuffers();
  while (aptMainLoop()) {
    gspWaitForVBlank();
    hidScanInput();

    u32 kDown = hidKeysDown();
    if (kDown & KEY_B)
      return;
  }
}

static SwkbdCallbackResult validate_addr(void *user, const char **ppMessage,
                                         const char *text, size_t textlen) {
  if (textlen < 8) {
    *ppMessage = "URL too short. Are you sure it's valid?";
    return SWKBD_CALLBACK_CONTINUE;
  } else if (strncmp(text, "http", 4) == 0) {
    return SWKBD_CALLBACK_OK;
  }
  *ppMessage = "Invalid URL. Please try again.";
  return SWKBD_CALLBACK_CONTINUE;
}

void set_test_server_addr() {
  static SwkbdState swkbd;
  static char buf[1024];
  SwkbdButton btn;

  swkbdInit(&swkbd, SWKBD_TYPE_WESTERN, 1, -1);
  swkbdSetValidation(&swkbd, SWKBD_NOTEMPTY_NOTBLANK, 0, 0);
  swkbdSetHintText(&swkbd, "Enter test server address");
  swkbdSetInitialText(&swkbd, test_server_addr ? test_server_addr : "http://");
  swkbdSetFilterCallback(&swkbd, validate_addr, NULL);
  btn = swkbdInputText(&swkbd, buf, sizeof(buf));

  if (btn == SWKBD_BUTTON_RIGHT) {
    if (test_server_addr)
      free(test_server_addr);
    test_server_addr = strdup(buf);
    show_message("Test server address set.\nPress B to go back.", true);
    return;
  }
  show_message("Cancelled.\nPress B to go back.", true);
}
