#ifndef TEST_COMMON_H
#define TEST_COMMON_H

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <3ds.h>

extern PrintConsole top;
extern PrintConsole bottom;
extern char *test_server_addr;

#define REQUIRE_TEST_SERVER_ADDR() if (!test_server_addr) { show_message("Please set test server address.", true); return; }
#define USER_AGENT "Mozilla/5.0 AppleWebKit/536.30 (KHTML, like Gecko) NX/3.0.0.5.23 Mobile"
#define GFX_REFRESH() gfxFlushBuffers(); gfxSwapBuffers();

struct select_menu {
  const char* name;
  void (*func)();
};

struct HTTPResult {
  u32 status;
  u8 *body;
  u32 size;
};

Result http_fetch(const char *url, struct HTTPResult *result);
Result http_download(const char *url, bool text);
void draw_menu(const struct select_menu *menu);
void show_message(const char *message, bool clear);
void set_test_server_addr();
// === Registered tests
void test_simple_http_lan();
void test_simple_http();
void test_simple_https();
void test_post_http();
void test_post_http_binary();
void test_speed_test_fastcom();

#endif